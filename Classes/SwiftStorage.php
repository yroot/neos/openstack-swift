<?php

namespace Yroot\OpenStack\Swift;


use OpenStack\ObjectStore\v1\Models\Container;
use OpenStack\ObjectStore\v1\Service as ObjectStoreService;
use OpenStack\ObjectStore\v1\Models\StorageObject as ObjectStoreObject;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\StreamWrapper;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\ResourceManagement\CollectionInterface;
use Neos\Flow\ResourceManagement\PersistentResource;
use Neos\Flow\ResourceManagement\ResourceManager;
use Neos\Flow\ResourceManagement\ResourceRepository;
use Neos\Flow\ResourceManagement\Storage\Exception;
use Neos\Flow\ResourceManagement\Storage\StorageObject;
use Neos\Flow\ResourceManagement\Storage\WritableStorageInterface;
use Neos\Flow\Utility\Environment;

/**
 * A resource storage based on OpenStack Object Storage
 */
class SwiftStorage implements WritableStorageInterface
{
    /**
     * Name which identifies this resource storage
     *
     * @var string
     */
    protected $name;

    /**
     * Name of the bucket which should be used as a storage
     *
     * @var string
     */
    protected $containerName;

    /**
     * A prefix to use for the key of bucket objects used by this storage
     *
     * @var string
     */
    protected $keyPrefix = '';

    /**
     * @Flow\Inject
     * @var Environment
     */
    protected $environment;

    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var ResourceRepository
     */
    protected $resourceRepository;

    /**
     * @Flow\Inject
     * @var OpenStackSwiftFactory
     */
    protected $openStackSwiftFactory;

    /**
     * @var ObjectStoreService
     */
    protected $objectStoreService;

    /**
     * @var Container
     */
    protected $currentContainer;

    /**
     * @Flow\Inject
     * @var \Psr\Log\LoggerInterface
     */
    protected $systemLogger;

    /**
     * Constructor
     *
     * @param string $name Name of this storage instance, according to the resource settings
     * @param array $options Options for this storage
     * @throws Exception
     */
    public function __construct($name, array $options = array())
    {
        $this->name = $name;
        $this->containerName = $name;
        foreach ($options as $key => $value) {
            switch ($key) {
                case 'container':
                    $this->containerName = $value;
                    break;
                case 'keyPrefix':
                    $this->keyPrefix = ltrim($value, '/');
                    break;
                default:
                    if ($value !== null) {
                        throw new Exception(sprintf('An unknown option "%s" was specified in the configuration of the "%s" resource GcsStorage. Please check your settings.', $key, $name), 1446667391);
                    }
            }
        }
    }

    /**
     * Initialize the Google Cloud Storage instance
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->objectStoreService = $this->openStackSwiftFactory->create();
    }

    /**
     * Returns the instance name of this storage
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the bucket name used as a storage
     *
     * @return string
     */
    public function getContainerName()
    {
        return $this->containerName;
    }

    /**
     * Returns the object key prefix
     *
     * @return string
     */
    public function getKeyPrefix()
    {
        return $this->keyPrefix;
    }

    /**
     * Imports a resource (file) from the given URI or PHP resource stream into this storage.
     *
     * On a successful import this method returns a PersistentResource object representing the newly
     * imported persistent resource.
     *
     * @param string | resource $source The URI (or local path and filename) or the PHP resource stream to import the resource from
     * @param string $collectionName Name of the collection the new PersistentResource belongs to
     * @return PersistentResource A resource object representing the imported resource
     * @throws \Neos\Flow\ResourceManagement\Storage\Exception
     */
    public function importResource($source, $collectionName)
    {
        $temporaryTargetPathAndFilename = $this->environment->getPathToTemporaryDirectory() . uniqid('Yroot_OpenStack_ObjectStorage_');

        if (is_resource($source)) {
            try {
                $target = fopen($temporaryTargetPathAndFilename, 'wb');
                stream_copy_to_stream($source, $target);
                fclose($target);
            } catch (\Exception $e) {
                throw new Exception(sprintf('Could import the content stream to temporary file "%s".', $temporaryTargetPathAndFilename), 1446667392);
            }
        } else {
            try {
                copy($source, $temporaryTargetPathAndFilename);
            } catch (\Exception $e) {
                throw new Exception(sprintf('Could not copy the file from "%s" to temporary file "%s".', $source, $temporaryTargetPathAndFilename), 1446667394);
            }
        }

        $resource = $this->importTemporaryFile($temporaryTargetPathAndFilename, $collectionName);
        unlink($temporaryTargetPathAndFilename);

        return $resource;
    }

    /**
     * Imports a resource from the given string content into this storage.
     *
     * On a successful import this method returns a PersistentResource object representing the newly
     * imported persistent resource.
     *
     * The specified filename will be used when presenting the resource to a user. Its file extension is
     * important because the resource management will derive the IANA Media Type from it.
     *
     * @param string $content The actual content to import
     * @param string $collectionName Name of the collection the new PersistentResource belongs to
     * @return PersistentResource A resource object representing the imported resource
     * @throws Exception
     * @api
     */
    public function importResourceFromContent($content, $collectionName)
    {
        $sha1Hash = sha1($content);
        $md5Hash = md5($content);
        $fileName = $sha1Hash;
        $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_buffer($fileInfo, $content);

        $resource = new PersistentResource();
        $resource->setFilename($fileName);
        $resource->setFileSize(strlen($content));
        $resource->setCollectionName($collectionName);
        $resource->setSha1($sha1Hash);
        $resource->setMd5($md5Hash);
        $resource->setMediaType($mimeType);

        $this->getCurrentContainer()->createObject([
            'name' => $this->keyPrefix . $sha1Hash,
            'content' => $content,
            'contentType' => $resource->getMediaType()
        ]);
        return $resource;
    }

    /**
     * Imports a resource (file) as specified in the given upload info array as a
     * persistent resource.
     *
     * On a successful import this method returns a PersistentResource object representing
     * the newly imported persistent resource.
     *
     * @param array $uploadInfo An array detailing the resource to import (expected keys: name, tmp_name)
     * @param string $collectionName Name of the collection this uploaded resource should be part of
     * @return string A resource object representing the imported resource
     * @throws Exception
     * @api
     */
    public function importUploadedResource(array $uploadInfo, $collectionName)
    {
        $pathInfo = pathinfo($uploadInfo['name']);
        $originalFilename = $pathInfo['basename'];
        $sourcePathAndFilename = $uploadInfo['tmp_name'];

        if (!file_exists($sourcePathAndFilename)) {
            throw new Exception(sprintf('The temporary file "%s" of the file upload does not exist (anymore).', $sourcePathAndFilename), 1446667850);
        }

        $newSourcePathAndFilename = $this->environment->getPathToTemporaryDirectory() . 'Yroot_OpenStack_ObjectStorage_' . uniqid() . '.tmp';
        if (move_uploaded_file($sourcePathAndFilename, $newSourcePathAndFilename) === false) {
            throw new Exception(sprintf('The uploaded file "%s" could not be moved to the temporary location "%s".', $sourcePathAndFilename, $newSourcePathAndFilename), 1446667851);
        }

        $sha1Hash = sha1_file($newSourcePathAndFilename);
        $md5Hash = md5_file($newSourcePathAndFilename);
        $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($fileInfo, $newSourcePathAndFilename);

        $resource = new PersistentResource();
        $resource->setFilename($originalFilename);
        $resource->setFileSize(filesize($newSourcePathAndFilename));
        $resource->setCollectionName($collectionName);
        $resource->setSha1($sha1Hash);
        $resource->setMd5($md5Hash);
        $resource->setMediaType($mimeType);

        $stream = new Stream(fopen($newSourcePathAndFilename, 'r'));

        $this->getCurrentContainer()->createObject([
            'name' => $this->keyPrefix . $sha1Hash,
            'stream' => $stream,
            'contentType' => $resource->getMediaType()
        ]);

        return $resource;
    }

    /**
     * Deletes the storage data related to the given PersistentResource object
     *
     * @param PersistentResource $resource The PersistentResource to delete the storage data of
     * @return bool TRUE if removal was successful
     * @throws \Exception
     * @api
     */
    public function deleteResource(PersistentResource $resource)
    {
        $this->getCurrentContainer()->getObject($this->keyPrefix . $resource->getSha1())->delete();

        return true;
    }

    /**
     * Returns a stream handle which can be used internally to open / copy the given resource
     * stored in this storage.
     *
     * @param PersistentResource $resource The resource stored in this storage
     * @return bool|resource A URI (for example the full path and filename) leading to the resource file or FALSE if it does not exist
     * @throws Exception
     * @api
     */
    public function getStreamByResource(PersistentResource $resource)
    {
        try {
            /** @var ObjectStoreObject $object */
            $object = $this->getCurrentContainer()->getObject($this->keyPrefix . $resource->getSha1());
            $this->systemLogger->info(sprintf("got %s %s", $object->name, $object->contentType));
            $stream = $object->download();
            return StreamWrapper::getResource($stream);
        } catch (\Exception $e) {
            $message = sprintf('Could not retrieve stream for resource %s (%s%s%s). %s', $resource->getFilename(), $this->containerName, $this->keyPrefix, $resource->getSha1(), $e->getMessage());
            $this->systemLogger->error($message);
            return false;
        }
    }

    /**
     * Returns a stream handle which can be used internally to open / copy the given resource
     * stored in this storage.
     *
     * @param string $relativePath A path relative to the storage root, for example "MyFirstDirectory/SecondDirectory/Foo.css"
     * @return bool|resource A URI (for example the full path and filename) leading to the resource file or FALSE if it does not exist
     * @throws Exception
     * @api
     */
    public function getStreamByResourcePath($relativePath)
    {
        try {
            /** @var ObjectStoreObject $object */
            $object = $this->getCurrentContainer()->getObject($this->keyPrefix . ltrim($relativePath, '/'));
            $stream = $object->download();
            return StreamWrapper::getResource($stream);
        } catch (\Exception $e) {
            $message = sprintf('Could not retrieve stream for resource (%s%s%s). %s', $this->containerName, $this->keyPrefix, $relativePath, $e->getMessage());
            $this->systemLogger->error($message);
            throw new Exception($message, 1446667861);
        }
    }

    /**
     * Retrieve all Objects stored in this storage.
     *
     * @return array<\Neos\Flow\ResourceManagement\Storage\StorageObject>
     * @api
     */
    public function getObjects()
    {
        $objects = array();
        foreach ($this->resourceManager->getCollectionsByStorage($this) as $collection) {
            $objects = array_merge($objects, $this->getObjectsByCollection($collection));
        }

        return $objects;
    }

    /**
     * Retrieve all Objects stored in this storage, filtered by the given collection name
     *
     * @param CollectionInterface $collection
     * @internal param string $collectionName
     * @return array<\Neos\Flow\ResourceManagement\Storage\StorageObject>
     * @api
     */
    public function getObjectsByCollection(CollectionInterface $collection)
    {
        $objects = array();
        $that = $this;
        $containerName = $this->containerName;
        $keyPrefix = $this->keyPrefix;
        $container = $this->getCurrentContainer();

        foreach ($this->resourceRepository->findByCollectionName($collection->getName()) as $resource) {
            /** @var \Neos\Flow\ResourceManagement\PersistentResource $resource */
            $object = new StorageObject();
            $object->setFilename($resource->getFilename());
            $object->setSha1($resource->getSha1());
            $object->setStream(function () use ($that, $containerName, $keyPrefix, $container, $resource) {
                return $container->getObject($keyPrefix . $resource->getSha1())->download();
            });
            $objects[] = $object;
        }

        return $objects;
    }

    /**
     * Imports the given temporary file into the storage and creates the new resource object.
     *
     * @param string $temporaryPathAndFilename Path and filename leading to the temporary file
     * @param string $collectionName Name of the collection to import into
     * @return PersistentResource The imported resource
     * @throws \Exception
     */
    protected function importTemporaryFile($temporaryPathAndFilename, $collectionName)
    {
        $sha1Hash = sha1_file($temporaryPathAndFilename);
        $md5Hash = md5_file($temporaryPathAndFilename);
        $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($fileInfo, $temporaryPathAndFilename);

        $resource = new PersistentResource();
        $resource->setFileSize(filesize($temporaryPathAndFilename));
        $resource->setCollectionName($collectionName);
        $resource->setSha1($sha1Hash);
        $resource->setMd5($md5Hash);
        $resource->setMediaType($mimeType);

        $container = $this->getCurrentContainer();
        if (!$container->objectExists($this->keyPrefix . $sha1Hash)) {

            $stream = new Stream(fopen($temporaryPathAndFilename, 'r'));

            $this->getCurrentContainer()->createObject([
                'name' => $this->keyPrefix . $sha1Hash,
                'stream' => $stream,
                'contentType' => $resource->getMediaType()
            ]);
            $this->systemLogger->info(sprintf('Successfully imported resource as object "%s" into bucket "%s" with MD5 hash "%s"', $sha1Hash, $this->containerName, $resource->getMd5() ?: 'unknown'));
        } else {
            $this->systemLogger->info(sprintf('Did not import resource as object "%s" into bucket "%s" because that object already existed.', $sha1Hash, $this->containerName));
        }

        return $resource;
    }

    /**
     * @return Container
     */
    protected function getCurrentContainer()
    {
        if ($this->currentContainer === null) {
            $this->currentContainer = $this->objectStoreService->getContainer($this->containerName);
        }
        return $this->currentContainer;
    }
}
