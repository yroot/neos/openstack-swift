<?php
namespace Yroot\OpenStack\Swift\Command;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Cli\CommandController;
use OpenStack\ObjectStore\v1\Models\Container;
use Yroot\OpenStack\Swift\OpenStackSwiftFactory;

/**
 * Swift command controller for the Yroot.OpenStack.Swift package
 *
 * @Flow\Scope("singleton")
 */
class OpenStackObjectStorageCommandController extends CommandController
{
    /**
     * @Flow\InjectConfiguration("profiles.default")
     * @var array
     */
    protected $defaultProfile;

    /**
     * @Flow\Inject
     * @var OpenStackSwiftFactory
     */
    protected $openStackSwiftFactory;

    /**
     * Checks the connection
     *
     * @param string $container If specified, we try to connect by retrieving meta data for this specific container only
     * @param string $prefix
     * @return void
     */
    public function connectCommand($container = null, $prefix = '')
    {
        try {
            $objectService = $this->openStackSwiftFactory->create('default');
            if ($container !== null) {
                $this->outputLine('Access list of objects in container "%s" with key prefix "%s" ...', [$container, $prefix]);

                $this->outputLine('Writing test object into container (%s) ...', [$container]);
                $objectService->getContainer($container)->createObject([
                    'name' => $prefix . 'Yroot.OpenStack.Swift.ConnectionTest.txt',
                    'content' => 'test'
                ]);
                $this->outputLine('Deleting test object from container ...');
                $objectService->getContainer($container)->getObject($prefix . 'Yroot.OpenStack.Swift.ConnectionTest.txt')->delete();
            } else {
                /** @var Container $container */
                foreach ($objectService->listContainers() as $container) {
                    echo $container->name . "<br>";
                }
            }
        } catch (\Exception $e) {
            $this->outputLine('<b>' . $e->getMessage() . '</b>');
            if ($container === null || $prefix === '') {
                $this->outputLine('No Containers found. Try using the "--container" and "--prefix" arguments.');
            }
            $this->quit(1);
        }
        $this->outputLine('OK');
    }
}
