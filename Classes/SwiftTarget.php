<?php
namespace Yroot\OpenStack\Swift;

use GuzzleHttp\Psr7\Stream;
use OpenStack\ObjectStore\v1\Models\Container;
use OpenStack\ObjectStore\v1\Service as ObjectStoreService;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\ResourceManagement\CollectionInterface;
use Neos\Flow\ResourceManagement\Exception;
use Neos\Flow\ResourceManagement\PersistentResource;
use Neos\Flow\ResourceManagement\Publishing\MessageCollector;
use Neos\Flow\ResourceManagement\ResourceManager;
use Neos\Flow\ResourceManagement\ResourceMetaDataInterface;
use Neos\Flow\ResourceManagement\Target\TargetInterface;
use OpenStack\ObjectStore\v1\Models\StorageObject as ObjectStoreObject;

/**
 * A resource publishing target based on OpenStack ObjectStorage
 */
class SwiftTarget implements TargetInterface
{

    /**
     * Name which identifies this resource target
     *
     * @var string
     */
    protected $name;

    /**
     * Name of the ObjectStorage container which should be used for publication
     *
     * @var string
     */
    protected $containerName;

    /**
     * A prefix to use for the key of container objects used by this storage
     *
     * @var string
     */
    protected $keyPrefix = '';

    /**
     * CORS (Cross-Origin Resource Sharing) allowed origins for published content
     *
     * @var string
     */
    protected $corsAllowOrigin = '*';

    /**
     * @var string
     */
    protected $baseUri;

    /**
     * Internal cache for known storages, indexed by storage name
     *
     * @var array<\Neos\Flow\ResourceManagement\Storage\StorageInterface>
     */
    protected $storages = array();

    /**
     * @Flow\Inject
     * @var \Psr\Log\LoggerInterface
     */
    protected $systemLogger;

    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var MessageCollector
     */
    protected $messageCollector;

    /**
     * @Flow\Inject
     * @var OpenStackSwiftFactory
     */
    protected $openStackSwiftFactory;

    /**
     * @var ObjectStoreService
     */
    protected $objectStoreService;

    /**
     * @var Container
     */
    protected $currentContainer;

    /**
     * @var array
     */
    protected $existingObjectsInfo;

    /**
     * Constructor
     *
     * @param string $name Name of this target instance, according to the resource settings
     * @param array $options Options for this target
     * @throws Exception
     */
    public function __construct($name, array $options = array())
    {
        $this->name = $name;
        foreach ($options as $key => $value) {
            switch ($key) {
                case 'container':
                    $this->containerName = $value;
                    break;
                case 'keyPrefix':
                    $this->keyPrefix = ltrim($value, '/');
                    break;
                case 'corsAllowOrigin':
                    $this->corsAllowOrigin = $value;
                    break;
                case 'baseUri':
                    $this->baseUri = $value;
                    break;
                default:
                    if ($value !== null) {
                        throw new Exception(sprintf('An unknown option "%s" was specified in the configuration of the "%s" resource GcsTarget. Please check your settings.', $key, $name), 1446719852);
                    }
            }
        }
    }

    /**
     * Initialize the Google Cloud Storage instance
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->objectStoreService = $this->openStackSwiftFactory->create();
    }

    /**
     * Returns the name of this target instance
     *
     * @return string The target instance name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the ObjectStorage object key prefix
     *
     * @return string
     */
    public function getKeyPrefix()
    {
        return $this->keyPrefix;
    }

    /**
     * Publishes the whole collection to this target
     *
     * @param CollectionInterface $collection The collection to publish
     * @throws Exception
     * @throws \Exception
     * @throws \Neos\Flow\Exception
     */
    public function publishCollection(CollectionInterface $collection)
    {
        if (!isset($this->existingObjectsInfo)) {
            $this->existingObjectsInfo = [];
            $parameters = [
                'prefix' => $this->keyPrefix
            ];


            /** @var ObjectStoreObject $storageObject */
            foreach ($this->getCurrentContainer()->listObjects($parameters) as $storageObject) {
                $this->existingObjectsInfo[$storageObject->name] = true;
            }
        }

        $obsoleteObjects = $this->existingObjectsInfo;
        $targetContainer = $this->getCurrentContainer();

        $storage = $collection->getStorage();
        if ($storage instanceof SwiftStorage) {
            $this->publishCollectionFromObjectStorage($collection, $storage, $this->existingObjectsInfo, $obsoleteObjects, $targetContainer);
        } else {
            foreach ($collection->getObjects() as $object) {
                /** @var \Neos\Flow\ResourceManagement\Storage\StorageObject $object */
                $this->publishFile($object->getStream(), $this->getRelativePublicationPathAndFilename($object), $object);
                unset($obsoleteObjects[$this->getRelativePublicationPathAndFilename($object)]);
            }
        }

        $this->systemLogger->info(sprintf('Removing %s obsolete objects from target container "%s".', count($obsoleteObjects), $this->containerName));
        foreach (array_keys($obsoleteObjects) as $relativePathAndFilename) {
            try {
                /** @var ObjectStoreObject $object */
                $object = $targetContainer->getObject($this->keyPrefix . $relativePathAndFilename);
                $object->delete();
            } catch (\Exception $e) {
                $this->systemLogger->error("Exception: %s", $e->getMessage());
            }
        }
    }

    /**
     * @param CollectionInterface $collection
     * @param SwiftStorage $storage
     * @param array $existingObjects
     * @param array $obsoleteObjects
     * @param Container $targetContainer
     * @throws Exception
     * @throws \Neos\Flow\Exception
     */
    private function publishCollectionFromObjectStorage(CollectionInterface $collection, SwiftStorage $storage, array $existingObjects, array &$obsoleteObjects, Container $targetContainer)
    {
        $storageContainerName = $storage->getContainerName();
        if ($storageContainerName === $this->containerName && $storage->getKeyPrefix() === $this->keyPrefix) {
            throw new Exception(sprintf('Could not publish collection %s because the source and target container is the same, with identical key prefixes. Either choose a different container or at least key prefix for the target.', $collection->getName()), 1446721125);
        }

        $storageContainer = $this->objectStoreService->getContainer($storageContainerName);
        $iteration = 0;

        $this->systemLogger->info(sprintf('Found %s existing objects in target container "%s".', count($existingObjects), $this->containerName));

        foreach ($collection->getObjects() as $object) {
            /** @var \Neos\Flow\ResourceManagement\Storage\StorageObject $object */
            $targetObjectName = $this->keyPrefix . $this->getRelativePublicationPathAndFilename($object);
            if (isset($existingObjects[$targetObjectName])) {
                $this->systemLogger->debug(sprintf('Skipping object "%s" because it already exists in container "%s"', $targetObjectName, $this->containerName));
                unset($obsoleteObjects[$targetObjectName]);
                continue;
            }

            $this->systemLogger->debug(sprintf('Copy object "%s" to container "%s"', $targetObjectName, $this->containerName));
            $options = [
                'destination' => $targetContainer->name . "/" . $targetObjectName,
                'contentType' => $object->getMediaType(),
            ];

            /** @var ObjectStoreObject $objectStoreObject */
            $objectStoreObject = $storageContainer->getObject($storage->getKeyPrefix() . $object->getSha1());
            $objectStoreObject->copy($options);
            $this->systemLogger->debug(sprintf('Successfully copied resource as object "%s" (MD5: %s) from container "%s" to container "%s"', $targetObjectName, $object->getMd5() ?: 'unknown', $storageContainerName, $this->containerName));
            unset($targetObjectName);
            $iteration++;
        }

        $this->systemLogger->info(sprintf('Published %s new objects to target container "%s".', $iteration, $this->containerName));
    }

    /**
     * Returns the web accessible URI pointing to the given static resource
     *
     * @param string $relativePathAndFilename Relative path and filename of the static resource
     * @return string The URI
     */
    public function getPublicStaticResourceUri($relativePathAndFilename)
    {
        if ($this->baseUri != '') {
            return $this->baseUri . $relativePathAndFilename;
        } else {
            /** @var ObjectStoreObject $object */
            $object = $this->getCurrentContainer()->getObject($this->keyPrefix . $relativePathAndFilename);
            return $object->getPublicUri()->__toString();
        }
    }

    /**
     * Publishes the given persistent resource from the given storage
     *
     * @param \Neos\Flow\ResourceManagement\PersistentResource $resource The resource to publish
     * @param CollectionInterface $collection The collection the given resource belongs to
     * @return void
     * @throws Exception
     * @throws \Exception
     * @throws \Neos\Flow\Exception
     */
    public function publishResource(PersistentResource $resource, CollectionInterface $collection)
    {
        $storage = $collection->getStorage();
        if ($storage instanceof SwiftStorage) {
            if ($storage->getContainerName() === $this->containerName && $storage->getKeyPrefix() === $this->keyPrefix) {
                throw new Exception(sprintf('Could not publish resource with SHA1 hash %s of collection %s because the source and target container is the same, with identical key prefixes. Either choose a different container or at least key prefix for the target.', $resource->getSha1(), $collection->getName()), 1446721574);
            }
            $targetObjectName = $this->keyPrefix . $this->getRelativePublicationPathAndFilename($resource);
            $storageContainer = $this->objectStoreService->getContainer($storage->getContainerName());

            $storageContainer->getObject($storage->getKeyPrefix() . $resource->getSha1())->copy([
                'destination' => $this->getCurrentContainer()->name . "/" . $targetObjectName,
                'contentType' => $resource->getMediaType(),
            ]);

            $this->systemLogger->debug(sprintf('Successfully published resource as object "%s" (MD5: %s) by copying from container "%s" to container "%s"', $targetObjectName, $resource->getMd5() ?: 'unknown', $storage->getContainerName(), $this->containerName));
        } else {
            $sourceStream = $resource->getStream();
            if ($sourceStream === false) {
                $this->messageCollector->append(sprintf('Could not publish resource with SHA1 hash %s of collection %s because there seems to be no corresponding data in the storage.', $resource->getSha1(), $collection->getName()), LOG_ERR, 1446721810);
                return;
            }
            $this->publishFile($sourceStream, $this->getRelativePublicationPathAndFilename($resource), $resource);
        }
    }

    /**
     * Unpublishes the given persistent resource
     *
     * @param \Neos\Flow\ResourceManagement\PersistentResource $resource The resource to unpublish
     * @throws \Exception
     */
    public function unpublishResource(PersistentResource $resource)
    {
        $objectName = $this->keyPrefix . $this->getRelativePublicationPathAndFilename($resource);
        $this->getCurrentContainer()->getObject($objectName)->delete();
        $this->systemLogger->debug(sprintf('Successfully unpublished resource as object "%s" (MD5: %s) from container "%s"', $objectName, $resource->getMd5() ?: 'unknown', $this->containerName));
    }

    /**
     * Returns the web accessible URI pointing to the specified persistent resource
     *
     * @param \Neos\Flow\ResourceManagement\PersistentResource $resource PersistentResource object or the resource hash of the resource
     * @return string The URI
     */
    public function getPublicPersistentResourceUri(PersistentResource $resource)
    {
        if ($this->baseUri != '') {

            return $this->baseUri . $this->encodeRelativePathAndFilenameForUri($this->getRelativePublicationPathAndFilename($resource));
        } else {
            /** @var ObjectStoreObject $object */
            $object = $this->getCurrentContainer()->getObject($this->keyPrefix . $this->getRelativePublicationPathAndFilename($resource));
            return $object->getPublicUri()->__toString();
        }
    }

    /**
     * Publishes the specified source file to this target, with the given relative path.
     *
     * @param resource $sourceStream
     * @param string $relativeTargetPathAndFilename
     * @param ResourceMetaDataInterface $metaData
     * @throws \Exception
     */
    protected function publishFile($sourceStream, $relativeTargetPathAndFilename, ResourceMetaDataInterface $metaData)
    {
        if(is_resource($sourceStream)) {
            $sourceStream = new Stream($sourceStream);
        }

        $objectName = $this->keyPrefix . $relativeTargetPathAndFilename;
        try {

            $object = $this->getCurrentContainer()->createObject([
                'name' => $objectName,
                'stream' => $sourceStream,
                'contentType' => $metaData->getMediaType()
            ]);

            $this->systemLogger->debug(sprintf('Successfully published resource as object "%s" in container "%s" with MD5 hash "%s"', $objectName, $this->containerName, $metaData->getMd5() ?: 'unknown'));
        } catch (\Exception $e) {
            $this->messageCollector->append(sprintf('Failed publishing resource as object "%s" in container "%s" with MD5 hash "%s": %s', $objectName, $this->containerName, $metaData->getMd5() ?: 'unknown', $e->getMessage()), \Neos\Error\Messages\Error::SEVERITY_WARNING, 1506847965352);
            if (is_resource($sourceStream)) {
                fclose($sourceStream);
            }
            return;
        }
    }

    /**
     * Determines and returns the relative path and filename for the given Storage Object or PersistentResource. If the given
     * object represents a persistent resource, its own relative publication path will be empty. If the given object
     * represents a static resources, it will contain a relative path.
     *
     * @param ResourceMetaDataInterface $object PersistentResource or Storage Object
     * @return string The relative path and filename, for example "c828d0f88ce197be1aff7cc2e5e86b1244241ac6/MyPicture.jpg"
     */
    protected function getRelativePublicationPathAndFilename(ResourceMetaDataInterface $object)
    {
        if ($object->getRelativePublicationPath() !== '') {
            $pathAndFilename = $object->getRelativePublicationPath() . $object->getFilename();
        } else {
            $pathAndFilename = $object->getSha1() . '/' . $object->getFilename();
        }
        return $pathAndFilename;
    }

    /**
     * Applies rawurlencode() to all path segments of the given $relativePathAndFilename
     *
     * @param string $relativePathAndFilename
     * @return string
     */
    protected function encodeRelativePathAndFilenameForUri($relativePathAndFilename)
    {
        return implode('/', array_map('rawurlencode', explode('/', $relativePathAndFilename)));
    }

    /**
     * @return Container
     */
    protected function getCurrentContainer()
    {
        if ($this->currentContainer === null) {
            $this->currentContainer = $this->objectStoreService->getContainer($this->containerName);
        }
        return $this->currentContainer;
    }
}
