<?php
namespace Yroot\OpenStack\Swift;

use GuzzleHttp\Client;
use OpenStack\Common\Transport\HandlerStack;
use OpenStack\Common\Transport\Utils as TransportUtils;

use OpenStack\Identity\v2\Service;
use OpenStack\OpenStack;
use Neos\Flow\Annotations as Flow;

/**
 * Factory for the OpenStack service class
 *
 * @Flow\Scope("singleton")
 */
class OpenStackSwiftFactory
{
    /**
     * @Flow\InjectConfiguration("profiles")
     * @var array
     */
    protected $credentialProfiles;

    /**
     * @Flow\Inject
     * @var \Neos\Flow\Utility\Environment
     */
    protected $environment;

    /**
     * Creates a new Storage instance and authenticates agains the OpenStack
     *
     * @param string $credentialsProfileName
     * @return \OpenStack\ObjectStore\v1\Service
     * @throws \Exception
     */
    public function create($credentialsProfileName = 'default')
    {
        if (!isset($this->credentialProfiles[$credentialsProfileName])) {
            throw new \Exception(sprintf('The specified OpenStack credentials profile "%s" does not exist, please check your settings.', $credentialsProfileName), 1446553024);
        }
        $profile = $this->credentialProfiles[$credentialsProfileName];

        // setup the client for OpenStack v1
        $openStack = new OpenStack($profile);

        return $openStack->objectStoreV1();
    }
}
