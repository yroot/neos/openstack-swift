# OpenStack Swift for Neos

**!!! This plugin is not completely tested !!!**

## Configuration
`Settings.yaml`
```yaml
Neos:
  Flow:
    resource:
      storages:
        swiftPersistentResourcesStorage:
          storage: 'Yroot\OpenStack\Swift\SwiftStorage'
          storageOptions:
            container: '{container}'
            #keyPrefix: '{keyPrefix}'

      collections:

      # Collection which contains all persistent resources
        persistent:
          storage: 'swiftPersistentResourcesStorage'
          target: 'swiftPersistentResourcesTarget'

      targets:
        localWebDirectoryPersistentResourcesTarget:
          target: 'Neos\Flow\ResourceManagement\Target\FileSystemTarget'
          targetOptions:
            path: '%FLOW_PATH_WEB%_Resources/Persistent/'
            baseUri: '_Resources/Persistent/'
            subdivideHashPathSegment: false

        swiftPersistentResourcesTarget:
          target: 'Yroot\OpenStack\Swift\SwiftTarget'
          targetOptions:
            container: '{container}'
            #keyPrefix: '{keyPrefix}'
            #baseUri: '{baseUri}'

Yroot:
  OpenStack:
    Swift:
      profiles:
        default:
          authUrl: '{authUrl}'
          region: '{region}'
          user:
            username: '{username}'
            password: '{password}'
          tenant:
            id: '{tenantId}'
            name: '{tenantName}'
```

## Thanks & credits

The base for this plugin is the [GCS Adaptor from Flownative](https://github.com/flownative/flow-google-cloudstorage)! 